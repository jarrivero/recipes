@extends('contracts.master')

@section('content')


<div class="row">

        {!! Form::open(['method'=>'PUT', 'Id'=>'contractForm',
                        'action'=>'Contracts\ContractController@update']) !!}

        @include('contracts.contract_form')

        <br/>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                {!! Form::submit('Guardar', ['class'=>'btn btn-success btn-block']) !!}
            </div>
        </div>

        {!! Form::close() !!}

</div>

    {{-- @include ('contracts.ajax_form') --}}

<script type="'text/javascript">

    {!! App\Classlib\Html\HtmlAjax::Autocomplete('/api/persons','IdPersContratada','IdPersContratada_Show') !!}

</script>




@stop