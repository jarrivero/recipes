@if ($atts = ['class'=>'form-control', 'required'=>'required'] )
@endif

<div class="row">
    <div class="col-md-2">
        <div class="form-group-sm">
            {!! Form::label('Id','Id') !!}
            {!! Form::text( 'Id',
                           empty($ct->Id) ? '' : $ct->Id,
                           ['class'=>'form-control','required'=>'required','readonly'=>'readonly']) !!}

        </div>
    </div>

    <div class="col-md-5">
        {!! Form::hidden('IdPersContratada',$ct->IdPersContratada,['id'=>'IdPersContratada','required'=>'required'])!!}
        <div class="form-group-sm">
            {!! Form::label('IdPersContratada_Show','Contratado') !!}
            {!! Form::text( 'IdPersContratada_Show',
                           empty($ct->Id) ? '' : $ct->responsible->Nombre.' '.$ct->employee->Apellidos,
                           array_merge(['id'=>'IdPersContratada_Show'],$atts)) !!}

        </div>
    </div>

    <div class="col-md-5">
        {!! Form::hidden('IdPersResp',$ct->IdPersResp,['id'=>'IdPersResp','required'=>'required'])!!}
        <div class="form-group-sm">
            {!! Form::label('IdPersResp_Show','Responsable') !!}
            {!! Form::text( 'IdPersResp_Show',
                            empty($ct->Id) ? '' : $ct->responsible->Nombre.' '.$ct->responsible->Apellidos,
                            array_merge(['Id'=>'IdPersResp_Show'],$atts)) !!}
        </div>
    </div>

</div>



<div class="row">
    <div class="col-md-2">
        <div class="form-group-sm">
            {!! Form::label('Convocatoria', 'Año') !!}
            {!! Form::select('Convocatoria',
                              $select['Convocatoria'],
                              $ct->Convocatoria,
                              ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group-sm">
            {!! Form::label('RefOficial', 'Referencia') !!}
            {!! Form::text('RefOficial',
                           $ct->RefOficial,
                           ['class'=>'form-control']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('TipoContrato','Modalidad') !!}
            {!! Form::select('TipoContrato',
                            $select['TipoContrato'],
                            $ct->TipoContrato,
                            $atts) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('IdContratoOrigen','Contrato Origen') !!}
            {!! Form::select('IdContratoOrigen',
                            $select['IdContratoOrigen'],
                            $ct->IdContratoOrigen,
                            ['class'=>'form-control']) !!}
        </div>
    </div>
</div>

@if (!empty($ct->Id))

    <div class="row">
        <div class="col-md-6">
            <div class="form-group-sm">
                {!! Form::label('IdDpto','Departamento') !!}
                {!! Form::select('IdDpto',
                            $select['IdDpto'],
                            $ct->IdDpartamento,
                            $atts) !!}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group-sm">
                {!! Form::label('IdAreaConocimiento','Área') !!}
                {!! Form::select('IdAreaConocimiento',
                            $select['IdAreaConocimiento'],
                            $ct->IdAreaConocimiento,
                            $atts) !!}
            </div>
        </div>
    </div>



@endif


<div class="row">
    <div class="col-md-6">
        <div class="form-group-sm">
            {!! Form::label('IdTipo','Tipo',['class'=>"control-label"]) !!}
            {!! Form::select('IdTipo',
                        $select['IdTipo'],
                        $ct->IdTipo,
                        $atts) !!}

        </div>
    </div>
    <div class="col-md-5">
        <div class="form-group-sm">
            {!! Form::label('IdCatLaboral','Categoria',['class'=>"control-label"]) !!}
            {!! Form::select('IdCatLaboral',
                            $select['IdCatLaboral'],
                            $ct->IdCatLaboral,
                            $atts) !!}

        </div>
    </div>


</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group-sm">
            {!! Form::label('ContratadoComo','Cont. como',['class'=>"control-label"]) !!}
            {!! Form::select('TipoJornada',
                            $select['TipoJornada'],
                            $ct->ContratadoComo,
                            $atts) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('TipoJornada','Jornada',['class'=>"control-label"]) !!}
            {!! Form::select('TipoJornada',
                            $select['TipoJornada'],
                            $ct->TipoJornada,
                            $atts) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('Turno', 'Turno',['class'=>"control-label"]) !!}
            {!! Form::select('TipoHorario',
                            $select['TipoHorario'],
                            $ct->TipoHorario,
                            $atts) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group-sm">
            {!! Form::label('DedicacionHoras', 'Horas') !!}
            {!! Form::text('DedicacionHoras', $ct->DedicacionHoras, $atts) !!}
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group-sm">
            {!! Form::label('DedicacionMinutos', 'Minutos') !!}
            {!! Form::text('DedicacionMinutos', $ct->DedicacionMinutos, $atts) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('HorasPeriodo', 'Periodo') !!}
            {!! Form::select('HorasPeriodo',
                            $select['HorasPeriodo'],
                            $ct->HorasPeriodo,
                            $atts) !!}
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-10">
        <div class="form-group-sm">
            {!! Form::label('Horario', 'Horario') !!}
            {!! Form::text('Horario',
                            $ct->Horario,
                            $atts) !!}
        </div>
    </div>
</div>

<div class="row">
    <!-- 'ImporteBrutoMes', 'ImporteFiniquito', 'ImporteTotal', -->
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('ImporteBrutoMes', 'Importe/Mes') !!}
            {!! Form::text('ImporteBrutoMes', $ct->ImporteBrutoMes, $atts) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('ImporteFiniquito', 'Finiquito') !!}
            {!! Form::text('ImporteFiniquito', $ct->ImporteFiniquito, $atts) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('ImporteTotal', 'Importe Total') !!}
            {!! Form::text('ImporteTotal', $ct->ImporteTotal, $atts) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('FechaInicio', 'Fecha Inicio') !!}
            {!! Form::date('FechaInicio', $ct->FechaInicio, $atts) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('FechaFin', 'Fecha Fin') !!}
            {!! Form::date('FechaFin',
                            $ct->FechaFin,
                            ['class'=>'form-control',
                             'min'=>$ct->FechaInicio,
                             'required'=>'required']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group-sm">
            {!! Form::label('EsCompatibilidad', '¿Compatibilidad?') !!}
            {!! Form::select('EsCompatibilidad',
                            array('No'=>'No','Si'=>'Si'),
                            $ct->EsCompatibilidad,
                            $atts) !!}
        </div>
    </div>
    <div class="col-md-10">
        <div class="form-group-sm">
            {!! Form::label('Observaciones', 'Observaciones') !!}
            {!! Form::textarea('Observaciones',
                            $ct->Observaciones,
                            ['class'=>'form-control', 'rows'=>'2']) !!}
        </div>
    </div>

</div>
