@extends('contracts.master')

@section('content')


    <style>
        .well {
            background: rgb(255, 255, 255);
        }
    </style>

    <div class="row">

        <div class="col-sm-2">
            <label>Id</label>

            <div class="well well-sm">{{ $ct->Id }}</div>
        </div>

        <div class="col-sm-5">
            <label>Contratado</label>

            <div class="well well-sm">{{ $ct->employee->FullName }}</div>
        </div>

        <div class="col-sm-5">
            <label>Responsable</label>

            <div class="well well-sm">{{ $ct->responsible->FullName }}</div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-2">
            <label>Convocatoria</label>

            <div class="well well-sm">{!! empty($ct->Convocatoria) ? '&nbsp;' :  $ct->Convocatoria !!}</div>
        </div>
        <div class="col-sm-4">
            <label>Referencia</label>

            <div class="well well-sm">{!! empty($ct->RefOficial) ? '&nbsp;' : $ct->RefOficial !!}</div>
        </div>
        <div class="col-sm-3">
            <label>Modalidad</label>

            <div class="well well-sm">{{ $ct->TipoContrato }}</div>
        </div>
        <div class="col-sm-3">
            <label>Contrato Origen</label>
            <div class="well well-sm">{{ $ct->IdContratoOrigen }}</div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-6">
            <label>Departamento </label>
            <div class="well well-sm">{{ $ct->department->Departamento }}</div>
        </div>
        <div class="col-sm-6">
            <label>Área</label>
            <div class="well well-sm">{!! empty($ct->knowledgeArea->Nombre)? '&nbsp;' : $ct->knowledgeArea->Nombre !!}</div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <label>Tipo</label>

            <div class="well well-sm">{{ $ct->type->Tipo }}</div>
        </div>
        <div class="col-sm-5">
            <label>Categoria</label>
            <div class="well well-sm">{{ $ct->category->CatLaboral }}</div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-4">
            <label>Cont. como</label>
            <div class="well well-sm">{{ $ct->ContratadoComo }}</div>
        </div>
        <div class="col-sm-3">
            <label>Jornada</label>
            <div class="well well-sm">{{ $ct->TipoJornada }}</div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <label>Turno</label>
            <div class="well well-sm">{{ $ct->TipoHorario }}</div>
        </div>
        <div class="col-sm-2">
            <label>Horas</label>

            <div class="well well-sm">{{ $ct->DedicacionHoras }}</div>
        </div>
        <div class="col-sm-2">
            <label>Minutos</label>

            <div class="well well-sm">{{ $ct->DedicacionMinutos }}</div>
        </div>
        <div class="col-sm-3">
            <label>Periodo</label>

            <div class="well well-sm">{{ $ct->HorasPeriodo }}</div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-10">
            <label>Horario</label>
            <div class="well well-sm">{{ $ct->Horario }}</div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <label>Importe/Mes</label>
             <div class="well well-sm">{{ $ct->ImporteBrutoMes }}</div>
        </div>
        <div class="col-sm-3">
            <label>Finiquito</label>
            <div class="well well-sm">{{ $ct->ImporteFiniquito }}</div>
        </div>
        <div class="col-sm-3">
            <label>Importe Total</label>
            <div class="well well-sm">{{ $ct->ImporteTotal }}</div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3">
            <label>Fecha Inicio</label>
            <div class="well well-sm">{{ $ct->FechaInicio }}</div>
        </div>
        <div class="col-sm-3">
            <label>Fecha Finalizacion</label>
            <div class="well well-sm">{{ $ct->FechaFin }}</div>
        </div>
        <div class="col-sm-3">
            <label>¿Compatibilidad?</label>
            <div class="well well-sm">{{ ($ct->EsCompatibilidad) ? 'Si' : 'No' }}</div>
        </div>
        <div class="col-sm-10">
            <label>¿Observaciones</label>
            <div class="well well-sm">{!! empty($ct->Observaciones) ? '&nbsp;' : $ct->Observaciones !!}</div>
        </div>

    </div>




@endsection
