<style>
    .celda_blanca {
        background: #FFFFFF;
        height: 30px;
        display: table-cell;
        vertical-align: bottom;
        float: none;
    }
    .celda_roja {
        background: #AD2624;
        height: 10px;
    }
    .celda_gris {
        background: #E6E6E6;
        height: 25px;
    }

</style>


<div class="row">
    <div class="col-xs-1">
        <a href="http://www.us.es" target="_blank"
               title="Enlace a la web de la Universidad de Sevilla. Se abre en una ventana nueva.">
            <img src="{{ asset('images/marca_mini.jpg') }}" alt="Logotipo de Universidad de Sevilla" class="img_header"/>
        </a>
    </div>
    <div class="col-xs-11">
        <div class="row celda_blanca">
            <strong>VICERRECTORADO DE INVESTIGACIÓN</strong>
        </div>
        <div class="row celda_roja">
        </div>
        <div class="row celda_gris">
            <img src="{{ asset('images/logo_mini_2.gif') }}" alt="Letras Universidad de Sevilla"/>
        </div>
    </div>
</div>
<br/>