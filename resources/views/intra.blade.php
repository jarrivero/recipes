<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Agency - Start Bootstrap Theme</title>
    <!-- Bootstrap Core CSS -->
    <link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <!-- Custom CSS -->

    <!-- Custom Fonts -->
    {!! Html::style('/fonts/font-awesome/css/font-awesome.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">
    <div class="row">
        @include('header')
    </div>
    <div class="row">
       @yield('topmenu')
    </div>
    <div class="row">
        <div class="col-xs-2">
            @yield('leftmenu')
        </div>
        <div class="col-xs-10">
            <div class="row">
                @yield('content')
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('/js/jquery-1.11.3.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('/js/bootstrap.min.js') }}"></script>

</body>
</html>