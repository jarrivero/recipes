<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Agency - Start Bootstrap Theme</title>
    <!-- Bootstrap Core CSS -->

    {!! Html::style('css/bootstrap.min.css') !!}
    {!! Html::style('css/bootstrap-theme.min.css') !!}
    <!-- Custom CSS -->

    <!-- Custom Fonts -->
    {!! Html::style('/fonts/font-awesome/css/font-awesome.min.css') !!}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    body {
        font-family: 'fontawesome';
    }

    .at-middle-page {
        position: absolute;
        top: 50%;
        left:50%;
        transform: translate(-50%,-50%);
        //border: 1px dashed deeppink;
    }
    .recipe-text {
        padding-top: 50px;
    }
</style>

<body id="page-top" class="index">

@include('recipes.navigation')

@include('recipes.header')

<br/><br/><br/><br/><br/>
<div class="container">
    @include('recipes.html.arroz_caldoso')
</div>

<!-- jQuery -->
{!! Html::script('js/jquery-1.11.3.min.js') !!}

<!-- Bootstrap Core JavaScript -->
{!! Html::script('js/bootstrap.min.js') !!}

</body>
</html>