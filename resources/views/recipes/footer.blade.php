<!-- jQuery -->
{!! Html::script('js/jquery-1.11.3.min.js') !!}

<!-- Bootstrap Core JavaScript -->
{!! Html::script('js/bootstrap.min.js') !!}


<!-- Custom javascript functions -->
{!! Html::script('js/recipes.js') !!}