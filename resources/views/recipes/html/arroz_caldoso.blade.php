<div id="arroz_caldoso_con_pollo" itemscope itemtype="http://data-vocabulary.org/Recipe">
    <span content="arroz,caldoso,pollo,verduras,refrito,sofrito" itemprop="keywords"></span>

    <div class="row recipe-title ">
        <div class="col-sd-8 col-sd-offset-2 bg-primary">
            <div class="row">
                <div class="col-xs-9 ">
                    <strong>Arroz caldoso con pollo</strong>
                </div>
                <div class="col-xs-3">
                    <div class="pull-right">
                        <button class="btn btn-block btn-primary SeeMore" data-toggle="collapse" data-target="#r1">
                            <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
             </div>
        </div>
    </div>
    <div class="row recipe-sumary">
        <div class="col-sd-3 col-sd-offset-2">
            <img class="img-responsive" alt="Arroz caldoso con pollo"
                 src="http://3.bp.blogspot.com/-lS-EohPitNQ/T2jT5t2tCjI/AAAAAAAABFI/hKw2wynqUvY/s450/arroz_caldoso_con_pollo.jpg"/>
        </div>
    </div>
    <div id="r1" class="row collapse recipe-body">
        <div class="col-sd-8 col-sd-offset-2">
            <p>
                <img class="img-responsive pull-right" alt="Arroz caldoso con pollo"
                     src="http://3.bp.blogspot.com/-lS-EohPitNQ/T2jT5t2tCjI/AAAAAAAABFI/hKw2wynqUvY/s450/arroz_caldoso_con_pollo.jpg"/>
                <span itemprop="summary">Esta es una de las comidas semanales en casa, es fácil de cocinar,
                    rápida y muy sana. Generalmente la hago con pollo pero con conejo esta mas buena, pruébala.
                    Si quieres que tus niños no te pongan pegas, puedes pasar por la batidora el refrito antes de
                    añadir la carne, así solo verán arroz y carne.</span>

            </p>

            <h3> Ingredientes</h3>
            <ul>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">400 gramos</span> de <span itemprop="name">pechuga de pollo</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">300 gramos</span> de <span itemprop="name">de arroz redondo o "bomba"</span>
                </li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">1 pimiento</span> <span itemprop="name">verde mediano</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">1 pimiento</span> <span itemprop="name">rojo mediano</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">1</span> <span itemprop="name"> tomate grande</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">2</span> <span itemprop="name">zanahorias</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">2</span> <span itemprop="name">dientes de ajo</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">1</span> <span itemprop="name">cebolla grande</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">700 gramos</span> de <span itemprop="name">agua</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="name">Colorante alimentario</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">1/2 cucharadita</span> de <span itemprop="name">pimentón dulce</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">Una pizca</span> de <span itemprop="name">Sal</span></li>
                <li itemprop="recipeIngredient">
                    <span itemprop="amount">3 cucharadas</span> de <span itemprop="name">Aceite de oliva</span></li>
            </ul>

            <br/> <a name='more'></a><br/>

            <h3> Preparación</h3> <br/>

            <div itemprop="instructions"><b>1.</b> Trocear la verdura.<br/> <br/>

                <div class="separator" style="clear: both; text-align: center;"><a
                            href="http://1.bp.blogspot.com/-F2blqd44onk/TxkgDcEyOGI/AAAAAAAAAcI/5BmfbyeSFpA/s900/sofrito_01.jpg"
                             style="margin-left: 1em; margin-right: 1em;"
                            title="ingredientes del arroz con pollo caldoso"><img
                                alt="ingredientes del arroz con pollo caldoso"
                                border="0"
                                src="http://1.bp.blogspot.com/-F2blqd44onk/TxkgDcEyOGI/AAAAAAAAAcI/5BmfbyeSFpA/s250/sofrito_01.jpg"/></a>
                </div>
                <br/> <b>2. </b> Troceamos la pechuga de pollo en pequeños dados y los rehogamos en una sartén con el
                aceite de oliva; sólo unas vueltas hasta que pierdan el color rosa de la carne de pollo. Apartamos este pollo en
                un plato.<br/> <br/>

                <div class="separator" style="clear: both; text-align: center;"><a
                            href="http://1.bp.blogspot.com/-SYL9ZoFE1Ww/TxkgDm4pXQI/AAAAAAAAAcI/-QTg_3VmL3Y/s900/arroz_con_pollo_01.jpg"
                            style="margin-left: 1em; margin-right: 1em;"
                            title="rehogar la carne de pollo"><img
                                alt="rehogar la carne de pollo" border="0"
                                src="http://1.bp.blogspot.com/-SYL9ZoFE1Ww/TxkgDm4pXQI/AAAAAAAAAcI/-QTg_3VmL3Y/s250/arroz_con_pollo_01.jpg"/></a>
                </div>
                <br/> En ese mismo aceite vamos a hacer un refrito que es la base de cualquier "guiso".<br/> <br/> <b>3.
                    Refrito:&nbsp;</b>troceamos toda la verdura y la apartamos. Añadimos una cuchara más de aceite&nbsp; y calentamos. Primero
                introducimos el ajo picado y la cebolla troceada. le damos una vueltas a fuego medio evitando que se dore, lo que queremos
                es que la verdura se ponga blanda y suelte su agua. A continuación el pimiento rojo, pimiento verde y la
                zanahoria.<br/> <br/>
                <br/>

                <div class="separator" style="clear: both; text-align: center;"><a
                            href="http://1.bp.blogspot.com/-e41rMknqrqM/TxkgEkMO7CI/AAAAAAAAAcI/X7OJIV1WiLM/s900/sofrito_02.jpg"
                            style="margin-left: 1em; margin-right: 1em;"
                            title="hacer el refrito, rehogar la verdura"><img alt="hacer el refrito, rehogar la verdura"
                                                                              border="0"
                                                                              src="http://1.bp.blogspot.com/-e41rMknqrqM/TxkgEkMO7CI/AAAAAAAAAcI/X7OJIV1WiLM/s250/sofrito_02.jpg"/></a>
                </div>
                <br/> Cuando el pimiento esté tierno, añadimos el tomate y un poco de sal.<br/> <br/>

                <div class="separator" style="clear: both; text-align: center;"><a
                            href="http://1.bp.blogspot.com/-BJ6DrDnfcXk/TxkgGBGceKI/AAAAAAAAAcI/vTuf3YX20pA/s900/sofrito_03.jpg"
                            style="margin-left: 1em; margin-right: 1em;"
                            title="hacer el refrito, añadir el tomate y salar"><img
                                alt="hacer el refrito, añadir el tomate y salar"
                                border="0"
                                src="http://1.bp.blogspot.com/-BJ6DrDnfcXk/TxkgGBGceKI/AAAAAAAAAcI/vTuf3YX20pA/s250/sofrito_03.jpg"/></a>
                </div>
                <br/> <b>4.&nbsp;</b>Este paso es opcional, pasar el refrito por la batidora para no encontrar trozos de
                verdura. Es cuestión de gusto. En casa lo pasamos.<br/> <br/> <b>5.&nbsp;</b>Una vez hecho esto volvemos a echar el
                refrito a la cazuela y le añadimos el pollo que refreímos al principio, se sazonan con las hierbas aromáticas.<br/>
                <br/>

                <div class="separator" style="clear: both; text-align: center;"><a
                            href="http://1.bp.blogspot.com/-RD-uvQ8jZ4Q/Txkm55xsBvI/AAAAAAAAAdI/ZkSal6T0ZYs/s900/arroz_con_pollo_02.jpg"
                           style="margin-left: 1em; margin-right: 1em;"
                            title="triturar el refrito y añadir el pollo"><img
                                alt="triturar el refrito y añadir el pollo"
                                border="0"
                                src="http://1.bp.blogspot.com/-RD-uvQ8jZ4Q/Txkm55xsBvI/AAAAAAAAAdI/ZkSal6T0ZYs/s250/arroz_con_pollo_02.jpg"/></a>
                </div>
                <br/> <br/> <b>6.&nbsp;</b>Añadimos el colorante, el pimentón, el arroz y le damos unas vueltas cuando
                coja calor añadimos el arroz y el agua (un poco mas del doble de agua que de arroz). Tan pronto rompa a hervir se
                baja el fuego al mínimo y se deja cocer 10 minutos. Se prueba de sal y se retoca al gusto, luego lo apagamos y se deja
                reposar 15 minutos antes de servir.<br/> <br/> <br/> ¡Buen provecho!.
            </div>
        </div>


    </div>
    <div class="col-md-3 col-md-offset-2">
        <button class="btn SeeMore" data-toggle="collapse" data-target="#r1">
            <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>
        </button>
    </div>

</div>

