@foreach($recipesList as $item)
    <div class="row recipe-text" id="recipe_{{$item->id}}">
        <div class="container">
            <div class="panel panel-default">
                <div class="panel-heading">{{ $item->name }}</div>
                <div class="panel-body">
                    {!! $item->text !!}
                </div>
            </div>
        </div>
    </div>
@endforeach