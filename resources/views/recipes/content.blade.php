<!!-- loading gif -->
<div id="loading_animation">
    <div class="container">
        <div class="at-middle-page">
            {!! Html::image('images/lg-loading.gif','loading....') !!}
        </div>
    </div>

</div>
<!!-- end loading gif -->


<!!-- Recipes List -->
<section>
    <div id="container">
        <div id="recipesList">

            <!-- Display list once loaded -->

        </div>
        <div class="col-md-4 col-md-offset-4" id="moreRecipes">
            <br/>
            <button class="btn btn-success btn-block">Cargar mas recestas</button>
        </div>
    </div>
</section>
<!!-- end Recipes List -->

<!!-- Recipes Text -->
<section>
    <div id="container">
        <div id="recipesText">

            <!-- Display text once loaded -->


        </div>
    </div>
</section>
<!!-- end Recipes Text -->