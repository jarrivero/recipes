@foreach($recipesList->chunk(2) as $items)
    <div class="row">
        <div clas="container"></div>
            @foreach($items as $item)
                <div class="col-sm-6 col-md-4">
                    <div class="clearfix">
                        <a href="#recipe_{{$item->id}}">
                            {!! Html::image($item->photo, $item->name, array('class'=>'img-responsive center-block')) !!}
                        </a>
                    </div>
                    <h3 class="text-center">{!! $item->name !!}</h3>
                </div>
            @endforeach
        </div>
    </div>
@endforeach