/**
 * Created by jose on 30/10/15.
 */

// global vars
var init = 0;
var step = 3;

/**
 * the two next function are necesary for body text
 * not overload top navigation bar
 */
$(window).resize(function () {
    $('body').css('padding-top', parseInt($('#topnavbar').css("height"))+10);
});

$(window).load(function () {
    $('body').css('padding-top', parseInt($('#topnavbar').css("height"))+10);
});


/**
 * get content from an ajax call
 * show a gif while it's getting the text
 */
$(window).load(function() {
    loadContent(init, step);
});

$("#moreRecipes").click(function() {
    init = init + step;
    step = 2;
    loadContent(init, step);
});
/**
 * load content from remote server
 * @param {int} init record to init
 * @param {int} step num records
 */
function loadContent(init, step)
{
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        method: "POST",
        url:  '/api/getRecipe',  // File that you're loading
        crossDomain: true,
        timeout: 3000,
        data: {_token: CSRF_TOKEN, init: init, step: step},  //laravel security for post
        dataType: "json",
        beforeSend: function(){
            showLoadingGif();
        },
        success: function(data){
            addContentList(data['list']);
            addContentText(data['text']);
        },
        error: function (xhr, ajaxOptions, thrownError) { //Add these parameters to display the required response
            alert('Error a obtener los datos');
            //alert(xhr.responseText);
        },
        complete: function() {
            hideLoadingGif();
        }
    });
}


/**
 * Show loading animation
 */
function showLoadingGif()
{
    $('#loading_animation').show();
}

/**
 * Hide loading animation
 */
function hideLoadingGif(){
    $('#loading_animation').hide();
}


/**
 * add content to id tag #recipesList
 * @param {string} htmltext - html text to add
 */
function addContentList(htmltext)
{
    $('#recipesList').append(htmltext);
}

/**
 * add content to id tag #recipesText
 * @param {string} htmltext - html text to add
 */
function addContentText(htmltext)
{
    $('#recipesText').append(htmltext);
}



function sleep(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
}



