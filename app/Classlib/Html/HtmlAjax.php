<?php

namespace app\Classlib\Html;


class HtmlAjax
{
	const SEARCH_VAR_NAME = 'txt2search';

    public static function Autocomplete($url, $idfieldname, $displayfieldname, $passvalues=array(), $minlength=2) {
        $pass='';
        foreach($passvalues as $key=>$value){
            $pass .= "$key:'$value', ";
        }
        $pass = substr($pass,0,-2);
		$pass = (!empty($pass)) ? ','.$pass : '';
        $dfn = str_replace('-','_',$displayfieldname);
        $clean_var_func_name = $dfn.'_clean_vars';
        $selected_value      = $dfn.'_value';

        return <<<EOF

var {$selected_value}="";
/* reset values */
/* this function is called when the delete key is pressed
	or not selected item */
function {$clean_var_func_name}(){
	$("#{$idfieldname}").val("");
	$("#{$displayfieldname}").val("");
	{$selected_value} = "";
}
$(function() {
	alert('hola caracola');
	$("#{$displayfieldname}")
	.focusout(function() {
		if ($("#{$displayfieldname}").val() != {$selected_value}){
			{$clean_var_func_name}();
		}
	})
	.keypress(function(event) {
		alert('adfasdf');
		if (event.which == 8){ //delete key
			{$clean_var_func_name}();
		}
	});
    $("#{$displayfieldname}").autocomplete({
        source: function (request, response) {
			$.ajax({
				url: "{$url}",
				type: "post",
				success: response,
				dataType: "json",
				data:{text2search:request.term {$pass} }
			});
		},
		selectFirst: true,
        minLength: {$minlength},
        autoFocus: true,
        select: function(event, ui) {
			$("#{$displayfieldname}").val(ui.item.value);
			$("#{$idfieldname}").val(ui.item.id);
			{$selected_value} = ui.item.label;
		},
        change: function(event, ui) {
			if (!ui.item) {
				{$clean_var_func_name}();
			}
		}
    });
});

EOF;


    }


}

