<?php

namespace App\Models\Contracts;

use App\Models\DbModel;

/**
 * App\Models\Contracts\Type
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Contracts\Contract[] $contracts
 */
class Type extends DbModel
{
    protected $connection = 'contracts';
    protected $table      = 'tipos_contratos';
    protected $primaryKey = 'Id';

    /**
     * Get contracts
     */
    public function contracts()
    {
        return $this->hasMany('App\Contracts\Contract', 'IdTipo', 'Id');
    }

}
