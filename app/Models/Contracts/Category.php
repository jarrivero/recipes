<?php

namespace App\Models\Contracts;

use App\Models\DbModel;

/**
 * App\Models\Contracts\Category
 *
 */
class Category extends DbModel
{
    protected $connection = 'contracts';
    protected $table      = 'catlaboral';
    protected $primaryKey = 'Id';


}
