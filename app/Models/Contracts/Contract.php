<?php

namespace App\Models\Contracts;

use App\Models\DbModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


/**
 * Class Contrato
 *
 * @package App\Models\Contracts
 * @property int    $Id                   id contrato
 * @property int    $IdPersContratada     id del contratado
 * @property int    $IdPersResp           id responsable del contrato
 * @property int    $IdOrgGestor          investigacion, fius, ...
 * @property int    $Convocatoria         año de convocatoria
 * @property string $RefOficial           referencia de contratoso oficiales
 * @property int    $TipoContrato         nuevo o prorroga
 * @property int    $IdContratoOrigen     id contrato del que es prorroga o proviene
 * @property int    $IdDpto               id departamento
 * @property int    $IdAreaConocimiento   id area de conocimiento
 * @property string $IdCentro             id centro
 * @property string $CentroExt            nombre de centro externo
 * @property int    $IdTipo               68/83, fpu, fpi, plan propio, ...
 * @property int    $IdCatLaboral         doctor, grado, tecnico especialista, ...
 * @property string $ContratadoComo       investigador o tecnico
 * @property string $TipoJornada          completa o parcia
 * @property string $TipoHorario          mañana, tarde, ...
 * @property int    $DedicacionHoras      numero de horas del periodo
 * @property int    $DedicacionMinutos    numero de minutos del periodo
 * @property string $HorasPeriodo         semana, mes, dia , año
 * @property string $Horario              horario de trabajo
 * @property float  $ImporteBrutoMes      importe bruto al mes
 * @property float  ImporteFiniquito      importe del finiquito
 * @property float  ImporteTotal          importe total del contrato
 * @property string $FechaIncio           fecha inicio del contrato
 * @property string $FechaFin             fecha finalizacion del contrato
 * @property bool   $EsCompatibilidad     indica si es compatible con otros contratos
 * @property string $Observaciones        notas al contrato
 * @property-read \App\Models\Persons\Person        $responsible
 * @property-read \App\Models\Persons\Person        $employee
 * @property-read \App\Models\Contracts\Type        $type
 * @property-read \App\Models\Contracts\Category    $category
 * @property-read \App\Models\Codes\KnowledgeArea   $knowledgeArea
 * @property-read \App\Models\Codes\Facility        $facility
 * @property-read \App\Models\Codes\Department      $department
 */
class Contract extends DbModel
{
    protected $connection = 'contracts';
    protected $table      = 'contratos';
    protected $primaryKey = 'Id';
    protected $fillable = [
        'IdPersContratada', 'IdPersResp', 'IdOrgGestor',
        'Convocatoria', 'RefOficial', 'TipoContrato', 'IdContratoOrigen',
        'IdAreaConocimiento', 'IdDpto', 'IdCentro', 'CentroExt',
        'IdTipo', 'IdCatLaboral','ContratadoComo', 'TipoJornada', 'TipoHorario', 'HorasDedicacion',
        'DedicacionMinutos', 'HorasPeriodo', 'Horario',
        'ImporteBrutoMes', 'ImporteFiniquito', 'ImporteTotal',
        'FechaInicio', 'FechaFin', 'FechaHominis', 'FechaElabContrato', 'FechaRetencionCredito',
        'FechaPresupuesto', 'FechaFirma', 'FechaNominas', 'FechaEnvioIntervencion',
        'FechaEnvioOrganismo', 'FechaRecepcionIntervencion', 'FechaAprobIntervencion',
        'FechaEmpresa', 'FechaInem', 'FechaEntradaRegistro', 'FechaSalida',
        'EsCompatibilidad', 'Observaciones'
    ];

    protected $dates = [
        'FechaInicio', 'FechaFin', 'FechaHominis', 'FechaElabContrato', 'FechaRetencionCredito',
        'FechaPresupuesto', 'FechaFirma', 'FechaNominas', 'FechaEnvioIntervencion',
        'FechaEnvioOrganismo', 'FechaRecepcionIntervencion', 'FechaAprobIntervencion',
        'FechaEmpresa', 'FechaInem', 'FechaEntradaRegistro', 'FechaSalida'];


    protected $rules = [
        'IdPersContratada' => 'required',
        'IdPersResp'       => 'required',
        'IdTipo'           => 'required',
        'IdOrgGestor'      => 'required',
        'Convocatoria'     => 'numeric',
        'ContratadoComo'   => 'required',
        'TipoJornada'      => 'required',
        'TipoHorario'      => 'required',
        'DedicacionHoras'  => 'required|between:1,60',
        'DedicacionMinutos'=> 'required|between:1,60',
        'HorasPeriodo'     => 'required',
        'Horario'          => 'required',
        'IdCatLaboral'     => 'required',
        'ImporteBrutoMes'  => 'required',
        'ImporteTotal'     => 'required',
        'ImporteRetenido'  => 'required',
        'Fecha_Inicio'     => 'required|date',
        'Fecha_Fin'        => 'required|date|after:FechaInicio',
    ];

    function getSelects()
    {
        return [
            'TipoJornada' => $this->getWorkdays(),
            'TipoHorario' => $this->getWorkShifts(),
        ];
    }

    /**
     * contract responsible, person model
     * @return BelongsTo
     */
    public function responsible()
    {
        return $this->belongsTo('App\Models\Persons\Person', 'IdPersResp', 'Id');
    }

    /**
     * employee of this contract
     * @return BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Persons\Person', 'IdPersContratada', 'Id');
    }

    /**
     * contract type model
     * @return BelongsTo
     */
    public function type()
    {
        return $this->belongsTo('App\Models\Contracts\Type', 'IdTipo', 'Id');
    }

    /**
     * contract category
     * @return BelongsTo
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Contracts\Category', 'IdCatLaboral', 'Id');
    }

    /**
     * knowledge area assigned to contract
     * @return BelongsTo
     */
    public function knowledgeArea()
    {
        return $this->belongsTo('App\Models\Codes\KnowledgeArea', 'IdAreaConocimiento', 'Id');
    }

    /**
     * facility where the employee go to work
     * @return BelongsTo
     */
    public function facility()
    {
        return $this->belongsTo('App\Models\Codes\Facility', 'IdCentro', 'IdCentro');
    }

    /**
     * department where the employee go to work
     * @return BelongsTo
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Codes\Department', 'IdDpto', 'Id');
    }

    /**
     * available types for contract
     * @return array
     */
    public function getTypes()
    {
        $tp = Type::all();
        return $tp->toArray();
    }

    /**
     * available modes for contract
     * @return array
     */
    public function getModes()
    {
        return array(2=>'Nuevo Contrato', 3=>'Prórroga');
    }

    /**
     * avalaible workdays
     * @return array
     */
    public function getWorkdays()
    {
        return array(2=>'Completa', 3=>'A Tiempo Parcial');
    }

    /**
     * available contract by
     * @return array
     */
    public function getContractBy()
    {
        return array('Personal Investigador'=>'Personal Investigador',
                     'Personal Científico o Técnico'=>'Personal Científico o Técnico');
    }

    /**
     * get available work categories
     * @return \ArrayIterator
     */
    public function getCategories()
    {
        return Category::all()->getIterator();
    }

    /**
     * get availaible shifts
     * @return array
     */
    public function getWorkShifts()
    {
        return array('Mañana'=>'Mañana','Tarde'=>'Tarde','Noche'=>'Noche',
                    'Mañana-Tarde'=>'Mañana-Tarde','Rotativo'=>'Rotativo');
    }

    /**
     * get available working days periodicity
     * @return array
     */
    public function getPeriodicity()
    {
        return array('Semana'=>'Semana', 'Día'=>'Día', 'Mes'=>'Mes', 'Año'=>'Año');
    }
}
