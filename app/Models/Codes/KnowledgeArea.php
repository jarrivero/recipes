<?php

namespace App\Models\Codes;

use App\Models\DbModel;

class KnowledgeArea extends DbModel
{
    protected $connection = 'memory';
    protected $table      = 'areas_conocimiento';
    protected $primaryKey = 'Id';
    protected $fillable   = [
        'NIF', 'Nombre', 'IdCodigoRama'
    ];


}
