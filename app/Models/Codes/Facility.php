<?php

namespace App\Models\Codes;

use App\Models\DbModel;

class Facility extends DbModel
{
    protected $connection = 'memory';
    protected $table      = 'centros';
    protected $primaryKey = 'Id';

}
