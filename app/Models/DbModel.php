<?php

namespace App\Models;

//use Illuminate\Database\Eloquent\Model;
use Eloquent;
use Validator;
use Schema;

/**
 * App\Models\DbModel
 *
 */
class DbModel extends Eloquent
{
    protected $dateFormat = 'd/m/Y';

    /** @var array rules array to store rules */
    protected $rules = array();

    /** @var  mixed var to store message errors */
    protected $errors;

    /**
     * validate the model
     * @param array $data
     * @return bool
     */
    public function validate($data)
    {
        // make a new validator object
        $v = Validator::make($data, $this->rules);

        // check for failure
        if ($v->fails())
        {
            // set errors and return false
            $this->errors = $v->errors();
            return false;
        }

        // validation pass
        return true;
    }

    /**
     * get validation errors
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * get model rules
     * @return array model rules
     */
    public function getRules()
    {
        return $this->rules;
    }

}