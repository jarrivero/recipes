<?php

namespace App\Models\Persons;

use App\Models\DbModel;

/**
 * App\Models\Persons\Person
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contracts\Contract[] $contractsResponsible
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Contracts\Contract[] $contractsEmployee
 * @property-read \App\Models\Persons\KnowledgeArea $knowledgeArea
 */
class Person extends DbModel
{
    protected $connection = 'memory';
    protected $table      = 'personal';
    protected $primaryKey = 'Id';
    protected $fillable   = [
        'NIF', 'Nombre', 'Apellido1', 'Apellido2'
    ];

    /**
     * Get contract where the person is responsible
     */
    public function contractsResponsible()
    {
        return $this->hasMany('App\Models\Contracts\Contract', 'IdPersResp', 'Id');
    }

    /**
     * Get contract where the person is employee
     */
    public function contractsEmployee()
    {
        return $this->hasMany('App\Models\Contracts\Contract', 'Id', 'IdPersContratada');
    }

    public function knowledgeArea()
    {
        return $this->belongsTo('App\Models\Persons\KnowledgeArea', 'IdAreaConocimiento', 'Id');
    }

    // setup accessor on the person
    public function getFullNameAttribute()
    {
        return $this->attributes['Nombre'] . ' ' . $this->attributes['Apellidos'];
    }

}
