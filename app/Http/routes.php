<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/* contratos */
Route::resource('contratos', 'Contracts\ContractController');
/*
Route::any ('contratos',            'Contracts\ContractController@index');
Route::any ('contratos/{id}',       'Contracts\ContractController@show');
Route::any ('contratos/{id}/edit',  'Contracts\ContractController@edit');
Route::post('contratos/update',     'Contracts\ContractController@update');
Route::post('contratos/delete/{id}','Contracts\ContractController@destroy');
*/

Route::get('/', function () {
    return view('welcome');
});

Route::any('api/persons',       'Api\ApiController@getAjaxPerson');

Route::any('index',       'Json\JsonController@index');
Route::any('ping',        'Json\JsonController@ping');
Route::any('get/recipe',  'Json\JsonController@htmlapp');


Route::post('api/getRecipe', array('middleware' => 'cors', 'uses' => 'Api\ApiController@getAjaxRecipe'));



Route::get('test/getRecipe', 'Api\ApiController@getAjaxRecipe');







