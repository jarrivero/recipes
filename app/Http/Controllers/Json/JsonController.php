<?php

namespace App\Http\Controllers\Json;

use App\Http\Controllers\Controller;
use App\Recipe;

use Request;
use Response;
use Input;
use View;

class JsonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        echo "hola ".__METHOD__;
        $this->getRecipes();
    }

    public function ping()
    {
       error_log(Input::get('_'));
       return Response::json(['success'])->setCallback(Input::get('callback'));

    }

    public function htmlapp()
    {
        return view('recipes.masterV2');
    }



    public function getAjaxRecipe()
    {
        $init = Request::input('init', 0);
        $step = Request::input('step', 2);
        error_log('getting recipes from '.$init. ' step '.$step, 0);

        error_log('hola caracola',0);

        return $this->getRecipes($init, $step);
        //Request::ajax()
        /*if(Request::pjax())
        {
            //$recipe = Recipe::find($id);

            //return Response::json(['data' => ['redirect' => '/']]);
            //return Response::json($recipe->Text);
            return $this->getRecipes($init, $step);

        }else
        {
            error_log('no es llamada ajax ...',0);
            return Response::json('POST');
            //return $this->index();
        }*/

    }

    /**
     * @param int $init
     * @param int $num
     * @return \Illuminate\Http\JsonResponse
     */
    private function getRecipes($init=0, $step=2)
    {
        $html = array();
        //get recipes
        $recipes = Recipe::orderBy('created_at', 'Desc')->take($step)->skip($init)->get();

        //make list
        $view = View::make('recipes.recipe_list');
        $view->with('recipesList', $recipes);
        $html['list'] = $view->render();

        //make text
        $view = View::make('recipes.recipe_text');
        $view->with('recipesList', $recipes);
        $html['text'] = $view->render();

        error_log($html['list'],0);
        return Response::json($html);
    }


    public function test()
    {
        $view = View::make('test');
        echo print_r(apache_get_modules(),1);
        echo $view->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /*** Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
