<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Recipe;

use App\Models\Persons\Person;

use Request;
use Response;
use Input;
use View;

class ApiController extends Controller
{

    public function index()
    {
        echo "hola ".__METHOD__;
    }


    public function getAjaxPerson()
    {
        $result = array();
        error_log('respondiendo a getAjaxPerson');
        $text2search = 'rivero';
        if (if(Request::ajax() && strlen($text2search))
        {
            $result = Person::whereRaw('CONCAT(Nombre," ",Apellidos) LIKE "%'.$text2search.'%"')
                            ->orderBy('Apellidos', 'ASC')
                            ->orderBy('Nombre','ASC')
                            ->get()
                            ->lists('Id','FullName')->take(20)->toArray();


        }
        return Response::json($result);
    }


    public function ping()
    {
       error_log(Input::get('_'));
       return Response::json(['success'])->setCallback(Input::get('callback'));

    }

    /**
     * https://laracasts.com/discuss/channels/requests/laravel-5-cors-headers-with-filters/?page=3
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAjaxRecipe()
    {
        //sleep(2);
        $init = Request::input('init', 0);
        $step = Request::input('step', 2);
        return $this->getRecipes($init, $step);
    }

    /**
     * @param int $init
     * @param int $num
     * @return \Illuminate\Http\JsonResponse
     */
    private function getRecipes($init=0, $step=2)
    {
        $html = array();
        //get recipes
        $recipes = Recipe::orderBy('created_at', 'Desc')->take($step)->skip($init)->get();

        //make list
        $view = View::make('recipes.recipe_list');
        $view->with('recipesList', $recipes);
        $html['list'] = $view->render();

        //make text
        $view = View::make('recipes.recipe_text');
        $view->with('recipesList', $recipes);
        $html['text'] = $view->render();

        //header("Access-Control-Allow-Origin: *");
        //echo json_encode($html);

        return Response::json($html);
    }


}
