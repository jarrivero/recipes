<?php

namespace App\Http\Controllers\Contracts;

use App\Http\Controllers\Controller;

use App\Classlib\Html\HtmlAjax;
use App\Models\Contracts\Contract;
use App\Models\Codes\Department;
use App\Models\Codes\KnowledgeArea;

use Request;
use Response;
use View;
use \Collective\Html\FormBuilder as Form;

class ContractController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return __CLASS__.' -- '.__FUNCTION__;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return $this->renderForm(new Contract());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
        return __CLASS__.' -- '.__FUNCTION__;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if ($id) {
            $contract = Contract::find($id);
        }else {
            die('necesario id');
        }


        $view = View::make('contracts.contract_show');
        $view->with('ct', $contract);

        echo $view->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $contract = Contract::findOrNew($id);
        error_log('edit id: ',$contract->Id,0);

        return $this->renderForm($contract);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $post = Request::all();

        $contract = Contract::find($post['Id']);

        if ($contract === null)
            return 'Error obteniendo el id de la base de datos';

        if (! $contract->validate($post))
            return $this->edit($post['Id']);

        $contract->fill($post)->save();

        return $this->show($contract->Id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * display form for edit or only read
     * @param Contract   $contract
     */
    private function renderForm($contract)
    {
        $view = View::make('contracts.contract_edit');
        $view->with('ct',    $contract);
        $view->with('select', $this->getSelects($contract));
        $view->with('ajax_idresp',
            HtmlAjax::Autocomplete('api/persons','IdPersContratada','IdPersContratada_Show'));

        echo $view->render();
    }

    protected function getPreviusContracts($employee_id, $current_id=0){
        $rst = Contract::where('IdPersContratada','=',$employee_id)
                            ->where('Id', '!=', $current_id)
                            ->orderBy('FechaInicio', 'desc')
                            ->take(10)
                            ->get();
        $before = array(''=>'');
        foreach($rst as $row) {
            $before[$row->Id] = $row->FechaInicio.' al '.$row->FechaFin;
        }
        return $before;
    }

    /**
     * get selects for the form
     * @param Contract $contract
     * @return array
     */
    protected function getSelects($contract){
        $select = array();

        //types
        $types = array(''=>'-seleccione uno-');
        foreach($contract->getTypes() as $row) {
            $types[$row['Id']] = $row['Tipo'];
        }
        $select['IdTipo'] = $types;

        //categories
        $categories=array(''=>'-seleccione uno-');
        foreach($contract->getCategories() as $row) {
            $categories[$row['Id']] = $row['CatLaboral'];
        }
        $select['IdCatLaboral'] = $categories;

        //years
        $years = array(''=>'');
        foreach(range(date('Y'),date('Y')-20) as $year) {
            $years[$year] = $year;
        }
        $select['Convocatoria'] = $years;

        //dptos
        $select['IdDpto'] = Department::where('Activo','=',1)
                                        ->orderBy('Departamento', 'asc')
                                        ->lists('Departamento', 'Id')
                                        ->toArray();

        //areas
        $select['IdAreaConocimiento'] = KnowledgeArea::all()
                                                    ->sortBy('Nombre')
                                                    ->lists('Nombre','Id')
                                                    ->toArray();

        //previous contracts
        if (!empty($contract->Id))
            $select['IdContratoOrigen'] = $this->getPreviusContracts($contract->IdPersContratada,$contract->Id);
        else
            $select['IdContratoOrigen'] = array();

        $select['TipoContrato']    = $contract->getModes();
        $select['TipoJornada']     = $contract->getWorkdays();
        $select['ContratadoComo']  = $contract->getContractBy();
        $select['TipoHorario']     = $contract->getWorkShifts();
        $select['HorasPeriodo']    = $contract->getPeriodicity();

        return $select;
    }
}
